@extends('layouts.app')

@section('content')
<h1 style="text-align: center">products</h1>
<a href="products/create" class="btn btn-primary" style="margin-bottom: 15px;">Create product</a>

    <div class="card">
        
                
        

        <table id="posts-table" class="table table-hover posts-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                
                <th scope="col">First name</th>
                <th scope="col">Last name</th>
                <th scope="col">Address</th>
                <th scope="col">Created_at</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                <tr>
                    <td> {{$product->id}}</td>
                    <td> {{$product->title}}</td>
                    <td> {{$product->text}}</td>
                    <td> {{$product->shortdescription}}</td>
                    <td> {{$product->created_at}}</td>

                    @csrf
                    
                    
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
                
    </div>
        
    
@endsection
