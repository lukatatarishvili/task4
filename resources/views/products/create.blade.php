@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Add product</h1>

        <form method="post" action="{{ action("ProductsController@store") }}">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="text">Text</label>
                <input type="text" name="text" id="text" class="form-control" >
            </div>
            <div class="form-group">
                <label for="shortdescription">short description</label>
                <textarea name="shortdescription" class="form-control" id="shortdescription" cols="30" rows="10"></textarea>
            </div>
            
            

            <input type="submit" class="btn btn-primary" value="submit">
            @csrf
        </form>
    
        
@endsection